namespace :app do
  desc "Bootstrap the application"
  task :bootstrap do
    system "bundle exec rake db:reset"
    system "FORCE=y BATCH=100 bundle exec rake environment elasticsearch:import:all"
  end
end
